package com.inbra.aqua.watson;


import android.app.Activity;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.ibm.watson.developer_cloud.text_to_speech.v1.TextToSpeech;
import com.ibm.watson.developer_cloud.text_to_speech.v1.model.SynthesizeOptions;
import com.ibm.watson.developer_cloud.text_to_speech.v1.util.WaveUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class TextSpeechDelegator {

    private TextToSpeech service;

    public void init() {
        this.service = new TextToSpeech("f584280f-cd46-42c2-b097-b04bf2679890", "cPAQXRsEHKNC");
        this.service.setEndPoint("https://stream.watsonplatform.net/text-to-speech/api");
    }

    public void synthesize(final String text, final Callback callback) throws Exception {
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            public InputStream in;
            public File file;

            @Override
            protected Void doInBackground(Void... voids) {

                try {
                    SynthesizeOptions synthesizeOptions = new SynthesizeOptions.Builder()
                            .text(text)
                            .voice(SynthesizeOptions.Voice.PT_BR_ISABELAVOICE)
                            .accept(SynthesizeOptions.Accept.AUDIO_WAV + ";rate=22050")
                            .build();

                    this.in = service.synthesize(synthesizeOptions).execute();


                } catch (Exception e) {
                    Log.e("aqua", e.getLocalizedMessage(), e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if (this.in != null) {
                    callback.audioMp3(this.in);
                }
            }
        };

        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    private void writeToFile(InputStream in, File file) {
        try {
            OutputStream out = new FileOutputStream(file);
            byte[] buf = new byte[10240];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            out.close();
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static interface Callback {
        public void audioMp3(InputStream in);
    }
}
