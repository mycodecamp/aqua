package com.inbra.aqua;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.inbra.aqua.animation.DragLayout;
import com.nostra13.universalimageloader.core.ImageLoader;


public class ImageCardFragment extends Fragment implements DragLayout.GotoDetailListener, DragLayout.ImageClickListener {
    private ImageView imageView;
    private TextView nameView;
    private String imageUrl;
    private DragLayout dragLayout;
    private SliderUpActivity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_imagecard, null);
        this.dragLayout = (DragLayout) rootView.findViewById(R.id.drag_layout);

        imageView = (ImageView) dragLayout.findViewById(R.id.fish_image);
        nameView = (TextView) dragLayout.findViewById(R.id.fish_name);

        // load image on view
        ImageLoader.getInstance().displayImage(imageUrl, imageView);

        dragLayout.setGotoDetailListener(this);
        dragLayout.setImageListener(this);

        return rootView;
    }


    @Override
    public void gotoDetail() {
       /* Activity activity = (Activity) getContext();
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity,
                new Pair(imageView, ChatActivity.IMAGE_TRANSITION_NAME),
                new Pair(nameView, ChatActivity.NAME_TRANSITION_NAME)
        );
        Intent intent = new Intent(activity, ChatActivity.class);
        intent.putExtra(ChatActivity.EXTRA_IMAGE_URL, imageUrl);
        ActivityCompat.startActivity(activity, intent, options.toBundle());
        */
       this.activity.showChatInfo(imageUrl);
    }

    @Override
    public void onClickEvent() {
        // this.activity.showButtons();
        gotoDetail();
    }

    public void bindData(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setActivity(SliderUpActivity activity) {
        this.activity = activity;
    }

    public void collapse() {
        this.dragLayout.collapse();
    }

    public void left() {
        this.dragLayout.opacityLeft();
    }

    public void right() {
        this.dragLayout.opacityRight();
    }

    public void center() {
        this.dragLayout.opacityNone();
    }

    public void expand() {
        this.dragLayout.expand();

    }
}
