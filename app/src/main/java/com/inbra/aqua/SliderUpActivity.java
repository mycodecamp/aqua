package com.inbra.aqua;

import android.Manifest;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioFormat;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.TooltipCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.inbra.aqua.animation.BounceInterpolator;
import com.inbra.aqua.animation.CustPagerTransformer;
import com.inbra.aqua.watson.AssistantDelegator;
import com.inbra.aqua.watson.SpeechTextDelegator;
import com.inbra.aqua.watson.StreamPlayer;
import com.inbra.aqua.watson.TextSpeechDelegator;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;
import omrecorder.AudioChunk;
import omrecorder.AudioRecordConfig;
import omrecorder.OmRecorder;
import omrecorder.PullTransport;
import omrecorder.PullableSource;
import omrecorder.Recorder;

public class SliderUpActivity extends AppCompatActivity {


    // components
    private ViewPager viewPager;
    private FloatingActionButton fab_mic_carrousel;
    private FloatingActionButton fab_send_carrousel;
    private FloatingActionButton fab_send_chat;
    private SlidingUpPanelLayout slider;
    private ImageView watsonView;
    private ImageView slider_arrow;
    private ImageView fish_image_chat;
    private EditText edit_text_chat;


    // dialogs
    private Dialog progressDialog;
    private Animation watsonAnimation;

    // mic
    private Recorder recorder;

    private List<ImageCardFragment> fragments;
    private ImageCardFragment currentFragment;

    // watson
    private SpeechTextDelegator speechText;
    private TextSpeechDelegator textSpeech;
    private AssistantDelegator assistant;

    // test images
    private final String[] imageArray = {"assets://image1.jpg", "assets://image2.jpg", "assets://image3.jpg", "assets://image4.jpg", "assets://image5.jpg"};
    private RecyclerView recyclerViewMessages;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slider);

        this.messages = new ArrayList<>();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = getWindow();
            window.setFlags(
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        // init
        this.speechText = new SpeechTextDelegator();
        this.speechText.init();

        this.textSpeech = new TextSpeechDelegator();
        this.textSpeech.init();

        this.assistant = new AssistantDelegator();
        this.assistant.init();


        // request permission
        requestPermission();

        // load imagens
        initImageLoader();

        // play welcome
        playWelcome();

        // ref a componentes
        viewPager = findViewById(R.id.viewpager_images);
        fab_mic_carrousel = findViewById(R.id.fab_btn_mic_carrousel);
        fab_send_carrousel = findViewById(R.id.fab_btn_send_carrousel);
        fab_send_chat = findViewById(R.id.fab_btn_send_chat);
        edit_text_chat = findViewById(R.id.edit_text_chat);

        slider = findViewById(R.id.slider_up_panel);
        slider_arrow = findViewById(R.id.slider_arrow);
        fish_image_chat = findViewById(R.id.fish_image_chat);


        // slider
        {
            slider.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
            slider.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
                @Override
                public void onPanelSlide(View panel, float slideOffset) {

                }

                @Override
                public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                    if (newState.equals(SlidingUpPanelLayout.PanelState.COLLAPSED)) {
                        //slider_arrow.setImageResource((R.drawable.ic_down_chevron));
                        changeImage(slider_arrow, R.drawable.ic_down_chevron);
                        currentFragment.gotoDetail();
                    }

                    if (newState.equals(SlidingUpPanelLayout.PanelState.EXPANDED)) {
                        //slider_arrow.setImageResource((R.drawable.ic_up_chevron));
                        changeImage(slider_arrow, R.drawable.ic_up_chevron);

                    }
                }
            });
        }

        // progress dialog with watson logo
        {
            progressDialog = new Dialog(this);
            progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            progressDialog.setContentView(R.layout.watson_layout);

            watsonAnimation = AnimationUtils.loadAnimation(this, R.anim.watson_animation);
            watsonAnimation.setRepeatCount(Animation.INFINITE);
            watsonAnimation.setInterpolator(new BounceInterpolator(0.2, 20));

            watsonView = (ImageView) progressDialog.findViewById(R.id.watson_icon);
        }

        // setting buttons
        {

            final Animation buttonAnimation = AnimationUtils.loadAnimation(this, R.anim.bounce);
            BounceInterpolator interpolator = new BounceInterpolator(0.2, 20);
            buttonAnimation.setInterpolator(interpolator);


            new SimpleTooltip.Builder(this)
                    .anchorView(fab_mic_carrousel)
                    .text("Presione para falar")
                    .gravity(Gravity.START)
                    .animated(true)
                    .transparentOverlay(false)
                    .build()
                    .show();

            fab_mic_carrousel.setOnTouchListener(new View.OnTouchListener() {

                private File audio_file = file();

                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (!fab_mic_carrousel.isClickable()) {
                        return false;
                    }


                    if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                        fab_mic_carrousel.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
                        fab_mic_carrousel.startAnimation(buttonAnimation);

                        {
                            recorder = OmRecorder.wav(
                                    new PullTransport.Default(mic(), new PullTransport.OnAudioChunkPulledListener() {
                                        @Override
                                        public void onAudioChunkPulled(AudioChunk audioChunk) {
                                            // animateVoice((float) (audioChunk.maxAmplitude() / 200.0));
                                        }
                                    }), audio_file);
                            recorder.startRecording();
                        }

                        return true;
                    }

                    if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                        fab_mic_carrousel.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(android.R.color.black)));
                        {
                            try {
                                recorder.stopRecording();
                                recorder = null;

                                Log.e("aqua", "file size: " + audio_file.length());

                                if ((audio_file.length() / 1000) < 100) {

                                    new SimpleTooltip.Builder(SliderUpActivity.this)
                                            .anchorView(fab_mic_carrousel)
                                            .text("Audio muito curto")
                                            .gravity(Gravity.START)
                                            .animated(true)
                                            .transparentOverlay(false)
                                            .build()
                                            .show();
                                    return true;
                                }

                                progressDialog.show();
                                watsonView.startAnimation(watsonAnimation);


                                speechText.recognize(audio_file, new SpeechTextDelegator.Callback() {
                                    @Override
                                    public void text(String text) {
                                        Log.i("aqua", "text: " + text);
                                        try {
                                            assistant.message(text, new AssistantDelegator.Callback() {
                                                @Override
                                                public void text(String text) {
                                                    try {
                                                        textSpeech.synthesize(text, new TextSpeechDelegator.Callback() {
                                                            @Override
                                                            public void audioMp3(InputStream in) {
                                                                progressDialog.dismiss();
                                                                // watsonView.clearAnimation();
                                                                play(in);
                                                                Runtime.getRuntime().gc();
                                                            }
                                                        });
                                                    } catch (Exception e) {
                                                        Log.e("aqua", e.getLocalizedMessage(), e);
                                                    }
                                                }
                                            });

                                        } catch (Exception e) {
                                            Log.e("aqua", e.getLocalizedMessage(), e);
                                        }
                                    }

                                    @Override
                                    public void error(String message) {
                                        Log.e("aqua", "error: " + message);
                                        progressDialog.dismiss();
                                    }
                                });


                            } catch (IllegalStateException e1) {
                                recorder = OmRecorder.wav(
                                        new PullTransport.Default(mic(), new PullTransport.OnAudioChunkPulledListener() {
                                            @Override
                                            public void onAudioChunkPulled(AudioChunk audioChunk) {
                                                // animateVoice((float) (audioChunk.maxAmplitude() / 200.0));
                                            }
                                        }), audio_file);
                            } catch (Exception e) {
                                Log.i("aqua", e.getLocalizedMessage(), e);

                            }
                        }


                        return true;
                    }
                    return false;
                }
            });

            fab_send_carrousel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //  currentFragment.gotoDetail();
                    slider.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                }
            });


            TooltipCompat.setTooltipText(fab_mic_carrousel, "Aperte para falar com o Watson");
            fab_send_chat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String text = edit_text_chat.getText().toString();

                    showMessage(text, Message.MESSAGE_TYPE.SENT);

                    edit_text_chat.setText("");

                    try {
                        assistant.message(text, new AssistantDelegator.Callback() {
                            @Override
                            public void text(String text) {
                                Log.i("aqua", "text:" + text);

                                showMessage(text, Message.MESSAGE_TYPE.RECEIVED);
                            }
                        });
                    } catch (Exception e) {
                        Log.e("aqua", e.getLocalizedMessage(), e);
                    }
                }
            });
        }

        // view pager
        {
            viewPager.setPageTransformer(false, new CustPagerTransformer(this));

            fragments = new ArrayList<>();
            for (int i = 0; i < 10; i++) {
                fragments.add(new ImageCardFragment());
            }

            viewPager.setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager()) {
                @Override
                public Fragment getItem(int position) {
                    // for test, get random fragment
                    final ImageCardFragment fragment = fragments.get(position % 10);
                    fragment.bindData(imageArray[position % imageArray.length]);
                    fragment.setActivity(SliderUpActivity.this);
                    return fragment;
                }

                @Override
                public int getCount() {
                    return 10;
                }
            });


            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(final int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(final int position) {
                    // show or hide bottom panel
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if ((position - 1) >= 0) {
                                fragments.get(position - 1).collapse();
                                fragments.get(position - 1).left();

                            }

                            currentFragment = fragments.get(position);
                            currentFragment.center();
                            currentFragment.expand();

                            if ((position + 1) < fragments.size()) {
                                fragments.get(position + 1).collapse();
                                fragments.get(position + 1).right();
                            }
                        }
                    });
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                }
            });

            // select default page
            viewPager.post(new Runnable() {
                @Override
                public void run() {
                    viewPager.setCurrentItem(1, false);
                }
            });

            // margins for show 3 views on page
            viewPager.setOffscreenPageLimit(3);
            viewPager.setPageMargin(
                    getResources().getDimensionPixelOffset(R.dimen.viewpager_margin));
        }

        // init message list
        {
            recyclerViewMessages = findViewById(R.id.rv_messages);

            LinearLayoutManager llm = new LinearLayoutManager(this);
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerViewMessages.setLayoutManager(llm);

            // test();

            recyclerViewMessages.setAdapter(new SliderUpActivity.MessagesAdapter(messages));
        }

        showButtons();


    }

    ///////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////

    private void showMessage(String text, Message.MESSAGE_TYPE type) {
        Message sent_message = new Message();
        sent_message.type = type;
        sent_message.message = text;

        messages.add(sent_message);

        recyclerViewMessages.getAdapter().notifyDataSetChanged();
        recyclerViewMessages.scrollToPosition(messages.size() - 1);
    }

    private void playWelcome() {
        try {
            this.assistant.message("ola", new AssistantDelegator.Callback() {
                @Override
                public void text(String text) {
                    try {
                        textSpeech.synthesize(text, new TextSpeechDelegator.Callback() {
                            @Override
                            public void audioMp3(InputStream in) {
                                play(in);
                            }
                        });
                    } catch (Exception e) {
                        Log.e("aqua", e.getLocalizedMessage(), e);
                    }
                }
            });
        } catch (Exception e) {
            Log.e("aqua", e.getLocalizedMessage(), e);
        }
    }

    public void showChatInfo(String imageUrl) {
        DisplayImageOptions options;
        options = new DisplayImageOptions.Builder()
                .displayer(new FadeInBitmapDisplayer(1000))
                .build();
        ImageLoader.getInstance().displayImage(imageUrl, fish_image_chat, options);


        slider.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);

        this.messages.clear();
        this.recyclerViewMessages.getAdapter().notifyDataSetChanged();

        // send message for new fish
        String message = "Nome do Peixe";
        showMessage(message, Message.MESSAGE_TYPE.SENT);
        try {
            this.assistant.message(message, new AssistantDelegator.Callback() {
                @Override
                public void text(String text) {
                    showMessage(text, Message.MESSAGE_TYPE.RECEIVED);
                }
            });
        } catch (Exception e) {
            Log.e("aqua", e.getLocalizedMessage(), e);
        }


    }

    public void showButtons() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                fab_mic_carrousel.setClickable(true);
                fab_mic_carrousel.setAlpha(1f);

                fab_send_carrousel.setClickable(true);
                fab_send_carrousel.setAlpha(1f);
            }
        });
    }


    public void hideButtons() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                fab_mic_carrousel.setClickable(false);
                fab_mic_carrousel.setAlpha(0.2f);

                fab_send_carrousel.setClickable(false);
                fab_send_carrousel.setAlpha(0.2f);
            }
        });
    }


    private void initImageLoader() {
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                this)
                .memoryCacheExtraOptions(480, 800)
                // default = device screen dimensions
                .threadPoolSize(3)
                // default
                .threadPriority(Thread.NORM_PRIORITY - 1)
                // default
                .tasksProcessingOrder(QueueProcessingType.FIFO)
                // default
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .memoryCacheSize(2 * 1024 * 1024).memoryCacheSizePercentage(13) // default
                .discCacheSize(50 * 1024 * 1024)
                .discCacheFileCount(100)
                .discCacheFileNameGenerator(new HashCodeFileNameGenerator()) // default
                .imageDownloader(new BaseImageDownloader(this)) // default
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple()) // default
                .writeDebugLogs().build();


        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);
    }

    private void changeImage(final ImageView view, final int resource) {
        final Animation anim_out = AnimationUtils.loadAnimation(this, android.R.anim.fade_out);
        final Animation anim_in = AnimationUtils.loadAnimation(this, android.R.anim.fade_in);

        anim_out.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setImageResource(resource);
                anim_in.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                    }
                });
                view.startAnimation(anim_in);
            }
        });
        view.startAnimation(anim_out);
    }


    private List<SliderUpActivity.Message> messages;

    public void test() {
        // test

        this.messages = new ArrayList<>();
        {
            SliderUpActivity.Message m = new SliderUpActivity.Message();
            m.message = "Ola Mundo";
            m.type = Message.MESSAGE_TYPE.SENT;
            this.messages.add(m);
        }
        {
            SliderUpActivity.Message m = new SliderUpActivity.Message();
            m.message = "texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto";
            m.type = Message.MESSAGE_TYPE.RECEIVED;
            this.messages.add(m);
        }


    }

    private void play(final InputStream in) {
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                StreamPlayer sp = new StreamPlayer();
                sp.playStream(in);
                return null;
            }
        };
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private PullableSource mic() {
        return new PullableSource.Default(
                new AudioRecordConfig.Default(
                        MediaRecorder.AudioSource.MIC, AudioFormat.ENCODING_PCM_16BIT,
                        AudioFormat.CHANNEL_IN_MONO, 44100
                )
        );
    }

    private File file() {
        return new File(Environment.getExternalStorageDirectory(), "aqua_message.wav");
    }

    ///////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////

    // message adapter
    public class MessagesAdapter extends RecyclerView.Adapter {
        private List<SliderUpActivity.Message> messages;

        public MessagesAdapter(List<SliderUpActivity.Message> messages) {
            this.messages = messages;
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView1 = null;

            if (viewType == Message.MESSAGE_TYPE.SENT.ordinal()) {
                itemView1 = LayoutInflater.from(SliderUpActivity.this)
                        .inflate(R.layout.chat_item_sent, parent, false);
            }

            if (viewType == Message.MESSAGE_TYPE.RECEIVED.ordinal()) {
                itemView1 = LayoutInflater.from(SliderUpActivity.this)
                        .inflate(R.layout.chat_item_rcv, parent, false);
            }
            return new SliderUpActivity.MessageHolder(itemView1);
        }

        @Override
        public int getItemViewType(int position) {
            return messages.get(position).type.ordinal();
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            SliderUpActivity.Message msg = messages.get(position);
            ((SliderUpActivity.MessageHolder) holder).message_text_view.setText(msg.message);
            if (msg.type == Message.MESSAGE_TYPE.RECEIVED) {
                ((SliderUpActivity.MessageHolder) holder).sender_text_view.setText("Watson");
            }
        }

        @Override
        public int getItemCount() {
            return messages.size();
        }

    }

    public class MessageHolder extends RecyclerView.ViewHolder {

        protected TextView message_text_view;
        protected TextView sender_text_view;

        public MessageHolder(View itemView) {
            super(itemView);
            message_text_view = itemView.findViewById(R.id.message_text_view);
            sender_text_view = itemView.findViewById(R.id.sender_text_view);
        }


    }

    // message data
    public static class Message {

        public static enum MESSAGE_TYPE {SENT, RECEIVED}

        public String message;
        public MESSAGE_TYPE type;

    }

    //

    private void requestPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO},
                    100);
        }
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    101);
        }

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    102);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.i("aqua", "permissions: " + (permissions.length > 0 ? permissions[0] : "") + " results: " + (grantResults.length > 0 ? grantResults[0] + "" : ""));
    }
}